package ExerciciosWeb;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GuiaLogin extends JFrame {

	private JTextField tfLogin;
	private JLabel lbSenha;
	private JLabel lbLogin;
	private JButton btLogar;
	private JButton btCancelar;
	private JPasswordField pfSenha;
	private static GuiaLogin frame;

	public GuiaLogin() {

		inicializarComponentees();
		definirEventos();

	}

	private void inicializarComponentees() {

		setTitle("Login do Sistema");
		setBounds(0, 0, 250, 200);
		setLayout(null);
		tfLogin = new JTextField(5);
		pfSenha = new JPasswordField();
		lbSenha = new JLabel("Senha");
		lbLogin = new JLabel("Login");
		btCancelar = new JButton("Cancelar");
		tfLogin.setBounds(100, 30, 120, 25);
		lbLogin.setBounds(30, 75, 80, 25);
		lbSenha.setBounds(30, 75, 80, 25);
		pfSenha.setBounds(100, 75, 120, 25);
		btLogar.setBounds(20, 120, 100, 25);
		btCancelar.setBounds(125, 120, 100, 25);
		add(tfLogin);
		add(lbSenha);
		add(lbLogin);
		add(btLogar);
		add(btCancelar);
		add(pfSenha);

	}

	private void definirEventos() {

		btLogar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				String senha = String.copyValueOf(pfSenha.getPassword());
				if (tfLogin.getText().equals("Java") && senha.equals(" Java")) {

					frame.setVisible(false);

				} else {

					JOptionPane.showMessageDialog(null, "Login ou senha incoretos");

				}

			}
		});

		btCancelar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}
		});

	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				frame = new GuiaLogin();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				Dimension tela = Toolkit.getDefaultToolkit().getScreenSize();
				frame.setLocation((tela.width - frame.getSize().height) / 2,
						(tela.height - frame.getSize().height) / 2);
				frame.setVisible(true);

			}
		});

	}

}
