package Cap02;

import javax.swing.JOptionPane;

public class SwitchCase {

	public static void main(String [] args) {

		String diaDaSemana = JOptionPane.showInputDialog(" Forne�a o um n�mero inteiro entre 1 e 7:");
		if (diaDaSemana != null) {
			try {

				int dia = Integer.parseInt(diaDaSemana);
				String extenso = "";
				switch (dia) {
				case 1:
					extenso = "Domingo";
					break;
				case 2:
					extenso = "Segunda-Feira";
					break;
				case 3:
					extenso = "Ter�a-Feira";
					break;
				case 4:
					extenso = "Quarta-Feira";
					break;
				case 5:
					extenso = "Quinta-Feira";
					break;
				case 6:
					extenso = "Sexta-Feira";
					break;
				case 7:
					extenso = "Sabado";
					break;
				}
				JOptionPane.showMessageDialog(null, "N�mero fornecido " + extenso);

			} catch (NumberFormatException erro) {
				JOptionPane.showMessageDialog(null, "Digite apenas valores n�mericos inteiros! \n" + erro.toString());
			}

		}
		System.exit(0);

	}

}
