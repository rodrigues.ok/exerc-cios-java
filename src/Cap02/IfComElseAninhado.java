package Cap02;

import javax.swing.JOptionPane;

public class IfComElseAninhado {

	public static void main(String[] args) {
		
		String diaDaSemana = JOptionPane.showInputDialog("Forne�a um valor inteiro entre 1  e 7");

		if (diaDaSemana != null) {

			try {
				int dia = Integer.parseInt(diaDaSemana);
				if (dia == 1)
					diaDaSemana = "Domingo";
				else if (dia == 2)
					diaDaSemana = "segunda - feira";
				else if (dia == 3)
					diaDaSemana = "ter�a - feira";
				else if (dia == 4)
					diaDaSemana = "quarta - feira";
				else if (dia == 5)
					diaDaSemana = "quinta - feira";
				else if (dia == 6)
					diaDaSemana = "sexta - feira";
				else if (dia == 7)
					diaDaSemana = "Sabado";
				else
					diaDaSemana = "Dia da Semana Desconhecido!";
				JOptionPane.showMessageDialog(null, diaDaSemana);

			} catch (NumberFormatException erro) {

				JOptionPane.showMessageDialog(null, "Digite apenas valores n�mericos - \n" + erro.toString());
				
			}
		}
		
		System.exit(0);

	}

}
